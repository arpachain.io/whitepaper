/** Written in Solidity-like pseudo-code. */
contract ArpaContract {
    struct Transaction {
        uint256 txnId;
        address dataConsumer;
        address dataProvider;
        string dataAddress;
        uint256 numParticipants;
        uint256 fund;
        bool isFinished;
    }
    
    struct MpcResult {
        // MPC may fail for various reasons, e.g. the fund is not enough to pay
        // compuation fee generated at run-time, or some parties aborted the
        // compuation.
        bool success;
        // MAC values are for MPC correctness verification.
        uint256[] macValues; 
    }
    
    mapping (uint256 => Transaction) public txns;
    uint256 public nextTxnId;

    event TransactionCreation(Transaction txn);
    event TransactionCompletion(Transaction txn);

    /**
     * Constructor function.
     */
    function ArpaContract() public {
        nextTxnId = 0;
    }
    
    /**
     * Data consumer calls this function to initiate an instance of MPC and
     * deposits fund for data usage and compuation fee.
     */
    function startTransaction(
        address dataConsumer,
        address dataProvider,
        string dataAddress,
        uint256 numParticipants,
        uint256 fund) public payable returns (bool success) {
        require(msg.value == fund);
        txnId = nextTxnId;
        nextTxnId += 1;
        txn = Transaction(
            txnId,
            dataConsumer,
            dataProvider,
            dataAddress,
            numParticipants,
            fund,
            /*isFinished=*/ false);
        txns[txnId] = txn;
        // Publish new transaction information to notify mpc network
        // to start compuation.
        emit TransactionCreation(txn);
        return true;
    }
    
    /**
     * The consensus layer of MPC network will call this function to report
     * completion of MPC and upload mpc result (including MAC values) for
     * verification.
     */
    function submitResult(uint256 txnId, MpcResult mpcResult)
        public returns (bool success) {
        txn = txns[txnId];
        require(!txn.isFinished);
        uint256 remainingFund = txn;
        
        if (verifyMpcResult(mpcResult)) {
            // Pay the fee to every party.
            remainingFund -= txn.dataUsageFee;
            txn.dataProvider.send(txn.dataUsageFee);
            for (worker : mpcResult.participants) {
                remainingFund -= worker.compensation;
                worker.address.send(worker.compensation)
            }
        }
        require(remainingFund >= 0);
        // If the data consumer deposited extra fund, or MPC verification
        // failed, refund the money to data consumer.
        if (remainingFund > 0) {
            dataConsumer.send(remainingFund);
        }
        
        txn.isFinished = true;
        // Publish new transaction information to notify mpc network
        // to start compuation.
        emit TransactionCompletion(txn);
        return true;
    }

    function verifyMpcResult(MpcResult mpcResult) public returns (bool success) {
        // Some MPC framework verifies MAC by checking (sum == 0).
        // This is only for illustration purpose.
        return mpcResult.success && sum(mpcResult.macValues) == 0;
    }
}