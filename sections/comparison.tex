\documentclass[../main.tex]{subfiles}

\begin{document}

\subsubsection{Trust Dependency}
Privacy-preserving computation can be sorted into two development directions: \textit{secure computation} and \textit{trusted computation}. The prior mainly focuses on the cryptography security brought by hard problems that are elaborately designed using mathematics. The security is derived purely from the number theory, which means even with unaffordable computing power, an adversary cannot break the encryption. The latter one starts from a different point, which tries to enforce the computing involvement to consistently behave in the expected way. Enforcing the behavior is achieved by hardware authentication, memory encryption, specific instruction design, etc. These technologies are always bound to the hardware platform such as the memory storage or processing elements.

Obviously, both Homomorphic Encryption and MPC belong to secure computation, and Trusted Execution Environment(TEE) is the effort of trusted computation. In brief, secure computation is more suitable to be used in permissionless network while trusted computation relies more on physical entities and can be used in a permissioned network such as Hyperledger\cite{turkihoneyledgerbft}.

\subsubsection{Scalability and Flexibility}
Given the foundation of mentioned computation methods, it can be observed that one can evaluate a secure computation on any computing power, either a data center or an edge device such as an automobile and phone. Due to the TEE's design methodology, users have to transplant the program from one platform to another by fitting their work into the safes. This feature makes the TEE less flexible than the pure protocol solutions. Considering the underlying device independency, MPC or HE can take advantage of mutual tool chains of hardware manufacturers and have future potentials for hardware accelerations.

As for different scenarios of computation, MPC can transform into a suitable pattern to make a trade-off between computation complexity and secureness. The nature of MPC can also satisfy various data-privacy input conditions. HE may support multiparty input as well by using distributed encryption and decryption, but the overhead of bootstrapping limits the secure functions evaluated on the HE.

\subsubsection{Practical Efficiency}

Thanks to the continuous effort of cryptographers and computer scientists in the last decade, different frameworks of MPC designed to solve various type of cases have been invented. We will develop a general purpose MPC network that achieves security in a malicious, dishonest, majority n-party setup.

Taking Intel SGX\cite{sgx} as the representative of TEE, the main overhead of SGX, compared with plaintext manipulating, is caused by fetching input from normal memory to the enclave local memory that involves encryption. According to the experiments on the enclaves, the Intel enclave instructions did not achieve the best performance. On the other hand, the optimal algorithm on plaintext cannot get the same throughput when processing enclave local memory. 

For S/FHE and MPC implementation, multiplication over ciphertext is the most frequently called and fundamental operation. Table \ref{tab:HE_vs_MPC} lists the state-of-the-art literal performance for both systems. The final column shows the latency of multiplication for different parameter settings. Although the diagram compares hardware implementation of SHE\cite{bonnoron2017somewhat} with practical software MPC\cite{keller2018overdrive}, it can be observed that MPC is 1-2 orders of magnitude faster than SHE. Considering in the future, dedicated hardware acceleration of MPC building blocks will be developed, and there should be another 10-100 times throughput improvement potential for MPC. Therefore, there exists 3-4 orders of magnitude of performance gap between HE and MPC. Furthermore, the most expensive operation of FHE is bootstrapping which enables unlimited multiplication depth is not listed in table. This may bring another slowdown in FHE computation.

\begin{table}[ht]
\centering
\begin{tabular}{*5c}   \toprule
Category     &   Scheme   &   \multicolumn{2}{c}{Security parameter} &       Operation   \\  \midrule
\multirow{5}{*}{HE} & \multicolumn{1}{m{3cm}}{Hardware implementation}   &    n   &   q     &   homomorphic multiplication \\  \cmidrule{2-5}
                    &   YASHE-NTT               &  4096  &   125 bits  &    6.5ms   \\
                    &   YASHE-NTT               &  16384(SIMD)  &  512 bits   &    48ms    \\
                    &   YASHE-NTT               &  32768(SIMD)  &  1228 bits  &    121ms    \\ \midrule
\multirow{4}{*}{MPC} &  Software implementation &  \multicolumn{2}{c}{Statistical security parameter}  &   Mult triples generations  \\ \cmidrule{2-5}
                     &  MPC                    &  \multicolumn{2}{c}{128 bits}                     &      0.23ms   \\
                     &  Overdrive: High Gear    &  \multicolumn{2}{c}{128 bits}                     &      0.43ms   \\
                     &  Overdrive: Low Gear     &  \multicolumn{2}{c}{128 bits}                     &      0.067ms  \\ \bottomrule
\end{tabular}
\caption{Performance Comparison of HE and MPC}
\label{tab:HE_vs_MPC}
\end{table}

\subsubsection{Conclusion}
In this section we discussed the history of MPC and its cryptographic properties and demonstrated that it is actually a very good candidate for mutually-distrusted parties to conduct secure computation together – a very similar setting to the blockchain network. 

We also visited several technologies currently existing as candidates of secure computation, namely Homomorphic Encryption, Zero-Knowledge Proof, and Trusted Execution Environment. Our discoveries indicate that, while some technologies have advantage such as computation efficiency, they are not offering security and features needed in a permissionless network. In essence, we need to be able to verify the secureness, correctness and privacy-preservingness of computation. Detailed aspects under consideration are as follows:

\paragraph{Efficiency} The speed of computation. Our experiments have found that, TEE is on par with clear text computation. Followed by MPC, about one magnitude slower, as forementioned. As for ZK-SNARK, it has around $10^3$ times overhead\cite{ben2013snarks}. The least favorable choice in terms of efficiency is FHE, with about $10^7$ to $10^8$ times slower.\cite{bonnoron2017somewhat}

\paragraph{Privacy-preserving} Privacy-preserving here refers to the capability of evaluating a function on a dataset, while not revealing the detail to anyone. This is the core of secure computation.

\paragraph{Proof of Correctness} Prove that the computation work is actually using the prescribed function.

In a trustless network, it is very important to prove that a certain function is evaluated in a correct way. In blockchain, it is simply done by repeated computation on every node running the smart contract. Consensus can be made once every participating node reaches on the same result. However, in the context of secure computation, computation is not carried out on every node but delegated to certain node(s) for the task. One needs to submit proof that computation has done the work with prescribed routine, or the result cannot be trusted. 

\paragraph{Proof of Computation} Prove the amount of work the participating node has done.

In Bitcoin and other PoW-style blockchain network, participating nodes prove their work by solving a hard problem (puzzle) and submit the hash value according to the nonce determined at the beginning of the block. It is trivial work for nodes to verify the result (hash) and prove that the node has indeed solved the puzzle. In a computation network, it is also important to know how much work has been done in the computation, so the node can be rewarded with a corresponding token. In MPC, the amount of computation can be proved by counting the triples consumed during the computation.

\paragraph{Proof of Secureness} Prove that the computation is actually carried out in the secured environment.

When computation is not repeated on every node, one needs to submit proof that the computation is carried out in a secured environment. Although this is what TEE is designed for, this seemingly naive question is actually very hard to prove. As mentioned earlier, secure computation is a cryptography process that guarantees the computation process is following the protocol or the result cannot be accepted. But trusted computation, on the other hand, enforces the process to be consistent with designed security (i.e. run the function) using trusted hardware. Nevertheless, in a permissionless network, one cannot convince other nodes to trust his result just by claiming it got remote attestation from a centralized server. Furthermore, even the remote attestation proves the security of the environment, one cannot prove the code is executed in that trusted environment.

A final conclusion with all facts related to blockchain construction is listed in table \ref{fig:secure_computation_comparison}.

\begin{table}[ht]
\centering
\begin{tabular}{*5c}   \toprule
        &	MPC	&	FHE	&	ZK-SNARK	&	TEE\\ \midrule
%Type	&	Secure Computation  &   Secure Computation	&	Zero Knowledge Proof	&	Trusted Execution	\\
Efficiency	&	Acceptable to Fast	&   Prohibitively Slow	&	Acceptable	&	Fast	\\
Privacy-preserving	&	Yes	& Yes	&	No	&	No 	\\
Trustless	&	Yes 	&	Yes	& 	No 	&	No	\\
Proof of Correctness	&	Yes	&   No     &	Yes	&   No	\\
Proof of Computation	&	Yes	&	No	&	No 	&   No	\\
Proof of Secureness	&	Yes	&	Yes	&	No 	&	No	\\ \bottomrule
\end{tabular}
\caption{Secure Computation and Trusted Execution}
\label{fig:secure_computation_comparison}
\end{table}


\end{document}

